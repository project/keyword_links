Keyword module for Drupal
=========================

Description:
------------
        
This module enables you to join nodes using keyword. Nodes
are linked using link list in body that is generated according
correspondent keywords of other nodes in order of relevance.
The main advantage of keywords that they enable bookmarking of nodes
that do not yet exist. Plan your keyword phrases in advance.

Keywords are entered at node edit page. Wildcards '*', '?' may be used.
Special tags 'S', 'T', 'P' forces that keyword is used only at target or
source side or link associated picture. Special chars may be quoted.
Quoted text is case sensitive. Keywords are delimited by comma,
semicolon, pipe or space. Underscores are changed to dash and percent
signs are omiited (in quoted text). Quoted wildcards still remain
wildcards.

Associated pictures in node listing are supported. 

For example:

Definition:

  Node1 keywords: alfa beta gama delta "Quoted CaSeSeNsItIvE keyword"
  Node2 keywords: alfa-romeo
  Node3 keywords: beta delta
  Node4 keywords: alfa|S alfa*
  Node5 keywords: alfa|T

Generated links:

  Node1 body: [Node3] [Node5]  # Node4 not listed due to 'S'
  Node2 body:
  Node3 body: [Node1] 
  Node4 body: [Node1] [Node2]
  Node5 body: # no nodes listed due to 'T' - there is no source keyword 

Note
----
API for import_export module is supported


Authors
-------
Tomas Mandys (tomas.mandys@2p.cz)

