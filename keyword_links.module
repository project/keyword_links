<?php

// keyword.flags
define('_KEYWORD_LINKS_DISABLE_MASTER', 0x01);
define('_KEYWORD_LINKS_DISABLE_SLAVE',  0x02);
define('_KEYWORD_LINKS_PICTURE',        0x04);


function keyword_links_help($section = "admin/help#keyword") {
  switch ($section) {
    case "admin/modules#description":
      return t("A keyword module that enables linking related nodes");
    case "admin/modules#name":
      return t("keyword_links");
  }
}

function keyword_links_settings() {
  $group = form_checkbox(t('Associated pictures'), 'keyword_links_associated_pictures', 1, variable_get('keyword_links_associated_pictures', 0), t('Check this box to render node associated pictures (image.module required).'));
  $group .= form_checkbox(t('Node galleries'), 'keyword_links_node_galleries', 1, variable_get('keyword_links_node_galleries', 0), t('Check this box to render node galleries (image.module required).')); 
  $group .= form_checkbox(t('Related nodes'), 'keyword_links_related_nodes', 1, variable_get('keyword_links_related_nodes', 0), t('Check this box to render related nodes.')); 
  $output = form_group(t('Rendering'), $group);
  
  return $output;
}

function _keyword_links_view(&$node, $teaser, $page) {
  if (variable_get('keyword_links_related_nodes', 0)) {
    $items = _keyword_links_get_target_list($node, (module_exist('image')&&variable_get('keyword_links_node_galleries', 0))?"<>'image'":'');  
    $node->body .= theme('keyword_links_list', $items);
  }
}

function _keyword_links_prepare(&$node, $teaser) {
  if (module_exist('image')) {   // prepare title picture
    if (variable_get('keyword_links_associated_pictures', 0) && $node->type!='image') {
      $items = _keyword_links_get_target_list($node, "='image'", _KEYWORD_LINKS_PICTURE);
      if (count($items)) {
        $nid = $items[0]['nid'];
        foreach ($items as $it) {   // preffer target with picture flag
          if ($it['flags'] & _KEYWORD_LINKS_PICTURE) {
            $nid = $it['nid'];
            break;
          }
        }
        $image = node_load(array('nid'=>$nid));
        $node->associated_picture = array();
        $node->associated_picture['filepath'] = file_create_path($image->images['thumbnail']);
        $node->associated_picture['title'] = $node->title;
        $node->associated_picture['alt'] = $image->title;
    //  $node->title .= theme_associated_picture($node); // alternate no theme approach
      }
    }
    if (!$teaser && variable_get('keyword_links_node_galleries', 0)) {
      $items = _keyword_links_get_target_list($node, "='image'");      
      $images = array();
      foreach ($items as $it) {
        $images[] = node_load(array('nid'=>$it['nid']));
      }
      $node->body .= theme('keyword_links_gallery', theme('image_gallery_album', $images, false)); // image theme return some obsolete table rows (see 'gallery_album_upper_row' string) 
    }
  }
}

function keyword_links_nodeapi(&$node, $op, $arg = 0, $arg2 = 0) {
  $default_fields = array(
    'keywords'=> array('type'=>IMPORT_EXPORT_ELEMENT, 'level'=>IMPORT_EXPORT_PUBLIC, 'flags'=>XML_FLAGS_BR)
  );
  switch ($op) { 
    case 'form post':
      $output = keyword_links_form($node); 
      break;
    case 'load':
      $output['keywords'] = _keyword_links_load($node);
      break;
    case 'insert':
    case 'update':
      _keyword_links_update($node);
      break;
    case 'delete':
      _keyword_links_delete($node);
      break;
    case 'view':
      _keyword_links_view($node, $arg, $arg2);
      break;
    case 'prepare':
      _keyword_links_prepare($node, $arg);
      break;
    case 'import_export':  // import_export module API hook
      switch ($arg) {
        case 'fields':
          import_export_add_fields($node, $arg2, $default_fields);
          break;
      }
      break;
  }
  return $output;
}

function keyword_links_menu() {

  $items[] = array('path' => 'keyword', 'title' => t('Keyword list'),
    'callback' => 'keyword_links_page',
    'access' => TRUE,
    'type' => MENU_CALLBACK);
  return $items;
}

function keyword_links_page() {
  switch (arg(1)) {
    case "list":
        print theme('page', _keyword_links_list(), t('Keyword list')); ;
      return;
  }
}

function keyword_links_form(&$node) {
  $output = form_textfield(t("Keywords"), "keywords", $node->keywords, 60, 128,
    t("Relating keyword to link similar nodes together using links").'<br />'.
    t("Format: <i>keyword1[\"|\"[S][T][P]] keyword2 keyword3</i>").'<br />'.
    t("Allowed delimiters: comma, semicolon, pipe, space. Wildcards (asterisk '*' or question mark '?') may be used. 'S' informs that the keyword won't be used as source one (linking from), 'T' informs that keyword won't as target one only (linking to). 'P'. keyword will be used for node picture. All keywords will be lowercased. Use quoutes '\"' for special chars (except wildcards).").'<br />'.
    l(t('See list of formerly defined keywords'), 'keyword/list')
  );
  return '<div class="attachments">'. form_group_collapsible(t('Keywords'), $output, TRUE, '') .'</div>';
}

function _keyword_links_add_quotes($s) {
  $s = preg_replace("'(_)'", '?', $s);
  $s = preg_replace("'(%)'", '*', $s);
  if (preg_match('/(,|;|\|| )/', $s) || mb_strtolower($s, 'UTF-8')!=$s)
    $s = '"'.$s.'"';
  return $s;
}

function _keyword_links_get_target_list(&$node, $type='', $prefered=0) {

  $items = array();
  $q = db_query("SELECT keyword FROM {keyword} WHERE nid='%d' AND flags & %d = %d", $node->nid, _KEYWORD_LINKS_DISABLE_MASTER|$prefered, $prefered);
  $s = '';      
  while ($r = db_fetch_array($q)) {
    if ($s!='') $s .= ' OR ';
    $s .= sprintf("k.keyword LIKE ('%s')", db_escape_string($r['keyword']));
  }
  if ($s=='') {
    return $items;
  }
  if ($type) {
    $type = ' AND n.type'.$type;
  }
  $q = db_query(db_rewrite_sql("SELECT n.nid,n.title,k.flags,COUNT(k.keyword) AS cnt FROM {node} n INNER JOIN {keyword} k ON n.nid=k.nid WHERE k.nid!='%d' AND ($s) AND k.flags & %d = 0 AND n.status = '1'". $type.' GROUP BY n.nid,n.title,k.flags ORDER BY cnt DESC, n.title ASC'), $node->nid, _KEYWORD_LINKS_DISABLE_SLAVE);
  while ($r = db_fetch_array($q)) {
    $items[] = $r; 
  }
  return $items;
}

function _keyword_links_array2string(&$arr) {
  $items = array();
  for ($i=0; $i<count($arr); $i++) {
    if (($s = $arr[$i]['keyword']) !='') {
      $s = _keyword_links_add_quotes($s);
      if ($arr[$i]['flags']) {
        $s .= '|';
      }
      if ($arr[$i]['flags'] & _KEYWORD_LINKS_DISABLE_MASTER) {
        $s .= 'S';
      } else if ($arr[$i]['flags'] & _KEYWORD_LINKS_DISABLE_SLAVE) {
        $s .= 'T';
      }
      if ($arr[$i]['flags'] & _KEYWORD_LINKS_PICTURE) {
        $s .= 'P';
      }
      $items[] = $s;
    }
  }
  return Implode(' ', $items);
}

function _keyword_links_string2array($s) {
 
  $s = preg_replace("'(_)'", '-', $s);  // change SQL changable wild chars
  $quo = '';
  $curs = '';
  $curflag = $curpar = 0;
  $res = array();
  $n = mb_strlen($s, 'UTF-8');
  for ($i=0; $i<$n; $i++) {
    $c = mb_substr($s, $i, 1, 'UTF-8');
    if ($c == '?') {   // wild chars non-quotable
      $curs .= '_';
    } 
    else if ($c == '*') {
      $curs .= '%';
    } 
    else if ($quo != '') {
      if ($c == $quo) {
        $quo = '';
      } 
      else if ($c == '%') {  // strip percent - SQL wildchar

      } 
      else {
        $curs .= $c;
      }
    } 
    else {
      if ($c == '"') {  // only qoutes, not apostroph to make easier reconstruction, otherwise should be handle mix of quotes and apostrophs
        $quo = $c;
      }
      else {
        if ($curpar) {
          switch ($c) {
            case 'S':
            case 's':
              $curflag |= _KEYWORD_LINKS_DISABLE_MASTER;
              break;
            case 'T':
            case 't':
              $curflag |= _KEYWORD_LINKS_DISABLE_SLAVE;
              break;
            case 'P':
            case 'p':
              $curflag |= _KEYWORD_LINKS_PICTURE;
              break;
          }
        }
        if ($c == '|') {
          $curpar = 1;
        }
        else if (preg_match('/(,|;|\|| )/', $c)) {
          if ($curs != '') {
            $res[] = array('keyword'=>$curs, 'flags'=>$curflag);
          }
          $curs = '';
          $curflag = $curpar = 0;
        } 
        else if (!$curpar) {
          $curs .= mb_strtolower($c, 'UTF-8');
        }
      }
    }
  }
  if ($curs != '') {
    $res[] = array('keyword'=>$curs, 'flags'=>$curflag);
  }
  $i = 0;
  while ($i < count($res)) {  // find duplicates
    $found = false;
    for ($j=0; $j<$i && !$found; $j++) {
      if ($found = $res[$i]['keyword'] == $res[$j]['keyword'])
        break;
    }
    if ($found) {
      $res[$j]['flags'] &= $res[$i]['flags'];
      Unset($res[$i]);
    } 
    else {
      $i++;
    }
  }
  return $res;
}

function _keyword_links_update($node) {
  $arr = _keyword_links_string2array($node->keywords);  // optimized for replication (not delete all & insert all)
  $dels = '';
  for ($i=0; $i<count($arr); $i++) {
    $q = db_query("SELECT * FROM {keyword} WHERE nid = '%d' AND keyword = '%s'", $node->nid, $arr[$i]['keyword']);
    if ($r = db_fetch_array($q)) {
      if ($r['flags'] != $arr[$i]['flags']) {
        db_query("UPDATE {keyword} SET flags = '%d' WHERE nid = '%d' AND keyword = '%s'", $arr[$i]['flags'], $node->nid, $arr[$i]['keyword']);
      }
    } 
    else {
      db_query("INSERT INTO {keyword} (nid, keyword, flags) VALUES ('%d', '%s', '%d')", $node->nid, $arr[$i]['keyword'], $arr[$i]['flags']);
    }
    if ($dels!='') $dels.=',';
    $dels .= "'".db_escape_string($arr[$i]['keyword'])."'";
  }
  if ($dels == '') {
    _keyword_links_delete($node);
  }
  else {
    db_query("DELETE FROM {keyword} WHERE nid = '%d' AND keyword NOT IN ($dels)", $node->nid);
  }
}

function _keyword_links_delete(&$node) {
  db_query("DELETE FROM {keyword} WHERE nid = '%d'", $node->nid);
}

function _keyword_links_load($node) {
  $q = db_query("SELECT * FROM {keyword} WHERE nid = '%d'", $node->nid);
  $res = array();
  while ($r = db_fetch_array($q)) {
    $res[] = $r;
  }
  return _keyword_links_array2string($res);
}

function _keyword_links_list() {

  $q = db_query("SELECT k.keyword,COUNT(k.keyword) AS cnt FROM {node} n INNER JOIN {keyword} k ON n.nid=k.nid GROUP BY k.keyword ORDER BY cnt DESC, k.keyword ASC");
  $rows = array();
  while ($r = db_fetch_array($q)) {
    $rows[] = array(_keyword_links_add_quotes($r['keyword']), $r['cnt']);
  }
  return theme('table', array(t('Keyword'), t('Count')), $rows);
}

function theme_keyword_links_list($links) {
  if (count($links) == 0) {
    return;
  }
  $items = array();
  for ($i=0; $i<count($links); $i++) {
    $items[] = l($links[$i]['title'], sprintf("node/%d", $links[$i]['nid']));
  }
  return '<div class="keyword_links"><div class="title">'. t('Related nodes') .':</div>'. theme("item_list", $items) .'</div>';
}

function theme_keyword_links_gallery($gallery) {
  if (!$gallery) {
    return;
  }
  return '<div class="keyword_links"><div class="title">'. t('Gallery') .':</div>'. $gallery .'</div>';
}

function theme_associated_picture($node) {
  if (variable_get('keyword_links_associated_pictures', 0) && $node->associated_picture) {  
    return theme('image', $node->associated_picture['filepath'], false, $node->associated_picture['title'], $node->associated_picture['alt'], false);
  }
}

?>
